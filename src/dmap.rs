use map::{Direction, Map};
use std::f32;
use std::fmt::Write;

pub type Grid = Vec<Vec<f32>>;

#[derive(Debug)]
pub struct DijkstraMapAtlas {
    pub dmaps: Vec<DijkstraMap>,
}

impl DijkstraMapAtlas {
    pub fn new() -> DijkstraMapAtlas {
        DijkstraMapAtlas {
            dmaps: Vec::new()
        }
    }

    pub fn add_dmap(&mut self, dmap: DijkstraMap) {
        self.dmaps.push(dmap);
    }
}

#[derive(Debug)]
pub struct DijkstraMap {
    pub name: String,
    pub width: u32,
    pub height: u32,
    pub tiles: Grid,
    pub goals: Vec<(i32, i32)>
}

impl DijkstraMap {
    pub fn new(w: u32, h: u32, name: String) -> DijkstraMap {
        DijkstraMap {
            name,
            width: w,
            height: h,
            tiles: vec![vec![f32::MAX; h as usize]; w as usize],
            goals: Vec::new()
        }
    }

    pub fn create_flee_map(&self, flee_coef: f32, map: &Map) -> DijkstraMap {
        let flee_grid = self.get_weighted_grid(flee_coef);
        let mut flee_map = DijkstraMap::new(self.width, self.height, format!("flee_{}", self.name));
        flee_map.seed_map(flee_grid);
        flee_map.dmap_algo(map);
        flee_map
    }

    pub fn clear_goals(&mut self) {
        self.goals.clear(); 
    }

    pub fn set_goal(&mut self, x: i32, y: i32) {
        self.goals.push((x,y));
    }

    pub fn seed_map(&mut self, seed: Grid) {
        self.tiles = seed;
    }

    pub fn compute(&mut self, map: &Map) {
        self.tiles = vec![vec![f32::MAX; self.height as usize]; self.width as usize];
        let goals = self.goals.clone();
        for goal in goals {
            self.tiles[goal.0 as usize][goal.1 as usize] = 0.0;
        }
        self.dmap_algo(map);
    }

    pub fn dmap_algo(&mut self, map: &Map) {
        let mut changed = true;
        while changed {
            changed = false;
            for i in 0..self.width {
                for j in 0..self.height {
                    if map.can_walk(i as i32,j as i32) {
                        let lowest_neighbor = self.get_neighbors_lowest_value(i as i32,j as i32);
                        if self.tiles[i as usize][j as usize] - lowest_neighbor >= 2.0 {
                            self.tiles[i as usize][j as usize] = lowest_neighbor + 1.0;
                            changed = true;
                        }
                    } 
                }
            }
        }
    }

    fn get_neighbors_lowest_value(&self, x: i32, y: i32) -> f32 {
        let mut lowest = f32::MAX;
        for i in 0..3 {
            let idx: i32 = x+i-1;
            if idx >= 0 && idx < self.width as i32 {
                let neighbor = self.tiles[idx as usize][y as usize];
                if neighbor < lowest {
                    lowest = neighbor;
                }
            }
        }
        for j in 0..3 {
            let idx: i32 = y+j-1;
            if idx >= 0 && idx < self.height as i32 {
                let neighbor = self.tiles[x as usize][idx as usize];
                if neighbor < lowest {
                    lowest = neighbor;
                }
            }
        }
        lowest
    }

    pub fn get_raw_grid(&self) -> Grid {
        self.tiles.clone()
    }

    pub fn get_weighted_grid(&self, weight: f32) -> Grid {
        let mut w_map = vec![vec![f32::MAX; self.height as usize]; self.width as usize];
        for i in 0..self.width {
            for j in 0..self.height {
                w_map[i as usize][j as usize] = self.tiles[i as usize][j as usize] * weight;
            }
        }
        w_map
    }

    pub fn dump_to_console(&self) {
        println!("===================================");
        for j in 0..self.height {
            let mut string: String = String::from("");
            for i in 0..self.width {
                let tile = self.tiles[i as usize][j as usize];
                let mut charac = String::from("");
                if tile >= 10.0 || tile <= -10.0 {
                    charac = String::from("X");
                } else {
                    write!(&mut charac, "{}", tile.floor() as i32);
                }
                string.push_str(&*charac);
            }
            println!("{:?}", string);
        }
        println!("===================================");
        println!(" ");
    }
}

#[derive(Debug)]
pub struct DGridProcessor {
    width: u32,
    height: u32,
    dgrids: Vec<Grid>
}

impl DGridProcessor {
    pub fn new(w: u32, h: u32) -> DGridProcessor {
        DGridProcessor {
            dgrids: Vec::new(),
            width: w,
            height: h,
        }
    }

    pub fn add_dmap(&mut self, dgrid: Grid) {
        self.dgrids.push(dgrid);
    }

    fn add_all_grids(&self) -> Grid {
        let mut final_grid = vec![vec![0.0; self.height as usize]; self.width as usize];
        let w = self.width;
        let h = self.height;
        for dgrid in self.dgrids.clone() {
            for i in 0..w {
                for j in 0..h {
                    final_grid[i as usize][j as usize] += dgrid[i as usize][j as usize];
                }
            }
        }
        final_grid
    }

    pub fn get_directions(&self, x: i32, y: i32) -> Direction {
        let final_grid = self.add_all_grids();
        let mut lowest = f32::MAX;
        let mut directions = Direction {x:0, y:0};
        for i in 0..3 {
            let idx: i32 = x+i-1;
            if idx >= 0 && idx < self.width as i32 {
                let neighbor = final_grid[idx as usize][y as usize];
                if neighbor < lowest {
                    lowest = neighbor;
                    directions.x = i-1;
                    directions.y = 0;
                }
            }
        }
        for j in 0..3 {
            let idx: i32 = y+j-1;
            if idx >= 0 && idx < self.height as i32 {
                let neighbor = final_grid[x as usize][idx as usize];
                if neighbor < lowest {
                    lowest = neighbor;
                    directions.x = 0;
                    directions.y = j-1;
                }
            }
        }
        directions
    }
}