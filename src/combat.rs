#[derive(Debug)]
pub struct CombatManager {
    event_queue: Vec<Combat>
}

impl CombatManager {
    pub fn new() -> CombatManager {
        CombatManager {
            event_queue: Vec::new()
        }
    }

    pub fn add_combat(&mut self, attacker: u32, defender: u32) {
        self.event_queue.push(Combat{attacker, defender});
    }

    pub fn next_combat(&mut self) -> Option<Combat> {
        self.event_queue.pop()
    }
}

#[derive(Debug)]
pub struct Combat {
    pub attacker: u32,
    pub defender: u32
}