use components::{Position, Player, TurnState, Movement, Fighter};
use actions::{ActionType, Action, Payload};
use tcod::input::{self, Event, Key};
use tcod::input::KeyCode::*;
use specs::{ReadStorage, WriteStorage, System, ReadExpect, WriteExpect, Entities};
use map::Map;
use fov::FovEngine;
use dmap::{DijkstraMapAtlas, DGridProcessor, Grid};
use TICK_ENERGY;
use combat::CombatManager;

pub struct ReplenishEnergy;

impl<'a> System<'a> for ReplenishEnergy {
    type SystemData = (WriteStorage<'a, TurnState>, ReadStorage<'a, Player>);

    fn run(&mut self, (mut turn_state, player) : Self::SystemData) {
        use specs::Join;
        let mut can_replenish = false;
        for (turn_state, _) in (&turn_state, &player).join() {
            if turn_state.energy < 0 {
                can_replenish = true;
            }
        }

        if can_replenish {
            for turn_state in (&mut turn_state).join() {
                turn_state.energy += TICK_ENERGY;
            }
        }
    }
}

pub struct ComputeDijkstraMap;

impl<'a> System<'a> for ComputeDijkstraMap {
    type SystemData = (ReadStorage<'a, Position>, ReadStorage<'a, Player>, ReadExpect<'a, Map>, WriteExpect<'a, DijkstraMapAtlas>, WriteExpect<'a, FovEngine>);

    fn run(&mut self, (positions, player, map, mut dmaps, fov) : Self::SystemData) {
        use specs::Join;
        let mut player_x = 0;
        let mut player_y = 0;
        for (position, _player) in (&positions, &player).join() {
            player_x = position.x;
            player_y = position.y;
        }
        for mut dmap in &mut dmaps.dmaps {
            dmap.clear_goals();
            let name = dmap.name.clone();
            match &name as &str{
                "player" => {
                    dmap.set_goal(player_x, player_y);
                }
                "stealth" => {
                    for i in 0..map.width {
                        for j in 0..map.height {
                            if !fov.is_in_fov(i, j) {
                                dmap.set_goal(i,j);
                            }
                        }
                    }
                }
                _ => {}
            }
            dmap.compute(&map);
        }
    }
}

pub struct ChooseAction;

impl<'a> System<'a> for ChooseAction {
    type SystemData = (WriteStorage<'a, TurnState>, ReadStorage<'a, Player>, ReadStorage<'a, Position>, ReadExpect<'a, Map>, WriteExpect<'a, DijkstraMapAtlas>);

    fn run(&mut self, (mut turn_state, player, position, map, mut dmaps) : Self::SystemData) {
        use specs::Join;

        for (turn_state, (), position) in (&mut turn_state, !&player, &position).join() {
            if turn_state.energy >= 0 {
                let mut processor = DGridProcessor::new(map.width as u32, map.height as u32);
                let mut player_dgrid = Grid::new();
                let mut stealth_dgrid = Grid::new();
                for mut dmap in &mut dmaps.dmaps {
                    let name = dmap.name.clone();
                    match &name as &str{
                        "player" => {
                            player_dgrid = dmap.get_weighted_grid(1.0);
                        }
                        "stealth" => {
                            stealth_dgrid = dmap.get_weighted_grid(2.5);
                        }
                        _ => {}
                    }
                }
                processor.add_dmap(player_dgrid);
                processor.add_dmap(stealth_dgrid);
                let directions = processor.get_directions(position.x, position.y);
                turn_state.next_action = Some(Action {
                    act_type: ActionType::Movement,
                    payload: Payload {dx:Some(directions.x),dy:Some(directions.y)},
                    energy_cost: 105
                });
            } else {
                turn_state.next_action = None;
            }
        }
    }
}

pub struct HandleInput;

impl<'a> System<'a> for HandleInput {
    type SystemData = (ReadStorage<'a, Player>, WriteStorage<'a, TurnState>);

    fn run(&mut self, (player, mut turn_state) : Self::SystemData) {
        use specs::Join;
        let mut action = None;
        let mut key = Default::default();

        match input::check_for_event(input::KEY_PRESS) {
            Some((_, Event::Key(k))) => key = k,
            _ => key = Default::default(),
        }

        match key {
            Key { code: Up, ..} => action = Some(Action {
                act_type: ActionType::Movement, 
                payload: Payload {dx:None, dy:Some(-1)},
                energy_cost: 100
            }),
            Key { code: Down, ..} => action = Some(Action {
                act_type: ActionType::Movement, 
                payload: Payload {dx:None, dy:Some(1)},
                energy_cost: 100
            }),
            Key { code: Left, ..} => action = Some(Action {
                act_type: ActionType::Movement, 
                payload: Payload {dx:Some(-1), dy:None},
                energy_cost: 100
            }),
            Key { code: Right, ..} => action = Some(Action {
                act_type: ActionType::Movement, 
                payload: Payload {dx:Some(1), dy:None},
                energy_cost: 100
            }),

            _ => {}                               
        }

        for (_, turn_state) in (&player, &mut turn_state).join() {
            turn_state.next_action = action;
        }
    }
}

pub struct Move;

impl<'a> System<'a> for Move {
    type SystemData = (Entities<'a>, WriteStorage<'a, Position>, WriteStorage<'a, Movement>, WriteStorage<'a, TurnState>, WriteExpect<'a, Map>, WriteExpect<'a, CombatManager>);

    fn run(&mut self, (entities, mut position, mut movement, mut turn_state, mut map, mut combat_manager): Self::SystemData) {
        use specs::Join;
        let pool = (&*entities, &mut position, &mut movement, &mut turn_state).join();
        for (self_entity, self_position, self_movement, self_turn_state) in pool {
            if self_turn_state.energy >= 0 {
                match self_turn_state.next_action {
                    Some(act) => {
                        match act.act_type {
                            ActionType::Movement => {
                                match act.payload.dx {
                                    Some(dx) => self_movement.dx = dx,
                                    _ => self_movement.dx = 0
                                }
                                match act.payload.dy {
                                    Some(dy) => self_movement.dy = dy,
                                    _ => self_movement.dy = 0
                                }
                            },
                            _ => {self_movement.dx = 0; self_movement.dy = 0}
                        }
                        self_turn_state.energy -= act.energy_cost;
                        self_turn_state.next_action = None;
                    }
                    _ => {}
                }

                if self_movement.dx != 0 || self_movement.dy != 0 {
                    match map.get_actor_at(self_position.x + self_movement.dx, self_position.y + self_movement.dy) {
                        Some(idx) => {
                            combat_manager.add_combat(self_entity.id(), idx);
                        }
                        None => {
                            if map.can_walk(self_position.x + self_movement.dx, self_position.y + self_movement.dy) {
                                map.clear_contained_actor(self_position.x, self_position.y);
                                self_position.x += self_movement.dx;
                                self_position.y += self_movement.dy;
                                map.set_contained_actor(self_position.x, self_position.y, self_entity.id());
                            }
                        }
                    }
                }
                
                self_movement.dx = 0;
                self_movement.dy = 0;
            }
        }
    }
}

pub struct ProcessCombat;

impl<'a> System<'a> for ProcessCombat {
    type SystemData = (Entities<'a>, WriteStorage<'a, Fighter>, WriteStorage<'a, Position>, WriteExpect<'a, CombatManager>, WriteExpect<'a, Map>);

    fn run(&mut self, (entities, mut fighter, mut position, mut combat_manager, mut map): Self::SystemData) {
        loop {
            let nxt_combat = combat_manager.next_combat();
            match nxt_combat {
                Some(combat) => {
                    let mut damage = 0;
                    {
                        let attacker = fighter.get_mut(entities.entity(combat.attacker)).unwrap();
                        damage = attacker.power;
                    }
                    let mut defender = fighter.get_mut(entities.entity(combat.defender)).unwrap();
                    defender.hp = defender.hp - damage;
                    if defender.hp <= 0 {
                        let mut pos = position.get_mut(entities.entity(combat.defender)).unwrap();
                        map.clear_contained_actor(pos.x, pos.y);
                        entities.delete(entities.entity(combat.defender)).unwrap();
                    }
                    println!("BAM! Dmg: {:?}", damage);
                }
                None => {break;}
            }
        }
    }
}
