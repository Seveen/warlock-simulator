use tcod::map::Map as FovMap;
use tcod::map::FovAlgorithm;
use std::sync::Mutex;
use map::Map;

pub struct FovEngine {
    pub fov_map: Mutex<FovMap>
}

unsafe impl Send for FovEngine {}
unsafe impl Sync for FovEngine {}

impl FovEngine {
    pub fn new(map: &Map) -> FovEngine {
        FovEngine {
            fov_map: Mutex::new(FovMap::new(map.width, map.height))
        }
    }

    pub fn generate_fov_map(&mut self, map: &Map) {
        for i in 0..map.width {
            for j in 0..map.height {
                let walkable = map.can_walk(i, j);
                let transparent = map.can_see(i, j);
                let mut fov = self.fov_map.lock().unwrap();
                fov.set(i, j, transparent, walkable);
            }
        }
    }

    pub fn compute_fov(&mut self, origin_x: i32, origin_y: i32, max_radius: i32, light_walls: bool, algo: FovAlgorithm) {
        let mut fov = self.fov_map.lock().unwrap();
        fov.compute_fov(origin_x, origin_y, max_radius, light_walls, algo);
    }

    pub fn is_in_fov(&self, x: i32, y: i32) -> bool {
        let fov = self.fov_map.lock().unwrap();
        fov.is_in_fov(x, y)
    }
}
