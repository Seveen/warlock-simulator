use components::{Glyph, Camera, Position};
use tcod::console::*;
use tcod::map::FovAlgorithm;
use tcod::colors;
use fov::FovEngine;
use map::Map;
use Continue;
use SCREEN_HEIGHT;
use SCREEN_WIDTH;
use specs::{ReadStorage, System, ReadExpect, WriteExpect};

pub struct DrawMap;

impl<'a> System<'a> for DrawMap {
    type SystemData = (ReadStorage<'a, Position>, ReadStorage<'a, Camera>, WriteExpect<'a, Map>, WriteExpect<'a, Root>, WriteExpect<'a, FovEngine>);

    fn run(&mut self, (position, camera, mut map, mut root, mut fov) : Self::SystemData) {
        use specs::Join;

        //Prefix c_ are console cells positions, w_ are world positions
        let c_center_x = SCREEN_WIDTH/2;
        let c_center_y = SCREEN_HEIGHT/2;

        //Get camera w_ pos
        let mut w_cam_x: i32 = 0;
        let mut w_cam_y: i32 = 0;
        for (position, _camera) in (&position, &camera).join() {
            w_cam_x = position.x;
            w_cam_y = position.y;
        }

        fov.compute_fov(w_cam_x, w_cam_y, 0, true, FovAlgorithm::Basic);

        //Calculate w_ coordinates for the cell at c_ [0.0]
        let w_origin_x = w_cam_x - c_center_x;
        let w_origin_y = w_cam_y - c_center_y;

        root.clear();
        for i in 0..SCREEN_WIDTH {
            for j in 0..SCREEN_HEIGHT {
                let w_x = w_origin_x + i;
                let w_y = w_origin_y + j;
                if w_x >= 0 && w_x < map.width && w_y >=0 && w_y < map.height {
                    let current_tile = map.tiles[w_x as usize][w_y as usize];
                    if fov.is_in_fov(w_x, w_y) {
                        map.explore(w_x, w_y);
                        root.set_default_foreground(current_tile.fg_color);
                        root.put_char(i, j, current_tile.character, BackgroundFlag::None);
                    } else if map.is_explored(w_x, w_y) {
                        root.set_default_foreground(colors::DARK_GREY);
                        root.put_char(i, j, current_tile.character, BackgroundFlag::None);
                    }
                }
            }
        }
    }
}

pub struct DrawActors;

impl<'a> System<'a> for DrawActors {
    type SystemData = (ReadStorage<'a, Position>, ReadStorage<'a, Glyph>, ReadStorage<'a, Camera>, WriteExpect<'a, Root>, WriteExpect<'a, Continue>, ReadExpect<'a, FovEngine>);

    fn run(&mut self, (position, glyph, camera, mut root, mut cont, fov) : Self::SystemData) {
        use specs::Join;
        //Prefix c_ are console cells positions, w_ are world positions.
        let c_center_x = SCREEN_WIDTH/2;
        let c_center_y = SCREEN_HEIGHT/2;

        //Get camera w_ pos.
        let mut w_cam_x: i32 = 0;
        let mut w_cam_y: i32 = 0;
        for (position, _camera) in (&position, &camera).join() {
            w_cam_x = position.x;
            w_cam_y = position.y;
        }

        for (position, glyph) in (&position, &glyph).join() {
            if fov.is_in_fov(position.x, position.y) {
                //w_cam == c_center (the camera is in the center of the screen)
                //Calculate delta between entity w_ position and camera w_ position
                let w_dx = position.x - w_cam_x;
                let w_dy = position.y - w_cam_y;
                //Calculate c_ coordinates
                let c_x = c_center_x + w_dx;
                let c_y = c_center_y + w_dy;
                if c_x >= 0 && c_x < SCREEN_WIDTH && c_y >=0 && c_y < SCREEN_HEIGHT {
                    root.set_default_foreground(glyph.fg_color);
                    root.put_char(c_x, c_y, glyph.character, BackgroundFlag::None);
                }
            }   
        }
        root.flush();

        if root.window_closed() {
            *cont = Continue(false);
        }
    }
}