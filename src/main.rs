extern crate tcod;
extern crate specs;
#[macro_use]
extern crate specs_derive;

mod actions;
mod components;
mod render_systems;
mod gameplay_systems;
mod map;
mod fov;
mod combat;
mod dmap;
use components::{Position, Glyph, Player, Camera, TurnState, Movement, Fighter};
use render_systems::{DrawMap, DrawActors};
use gameplay_systems::{ReplenishEnergy, ComputeDijkstraMap, ChooseAction, HandleInput, Move, ProcessCombat};
use map::MapMaker;
use fov::FovEngine;
use combat::CombatManager;
use dmap::{DijkstraMap, DijkstraMapAtlas};

use tcod::console::*;
use tcod::colors;
use specs::{World, DispatcherBuilder};

const SCREEN_WIDTH: i32 = 80;
const SCREEN_HEIGHT: i32 = 50;
const LIMIT_FPS: i32 = 20;
const TICK_ENERGY: i32 = 100;

fn main() {
    //Root console init
    let root = Root::initializer()
        .font("Kai16x16.png", FontLayout::AsciiInRow)
        .font_type(FontType::Default)
        .size(SCREEN_WIDTH, SCREEN_HEIGHT)
        .title("WarlockSimulator")
        .init();

    tcod::system::set_fps(LIMIT_FPS);

    //Specs world init
    let mut world = World::new();
    world.register::<Position>();
    world.register::<Glyph>();
    world.register::<Player>();
    world.register::<Camera>();
    world.register::<TurnState>();
    world.register::<Movement>();
    world.register::<Fighter>();

    world.add_resource(root);
    world.add_resource(Continue(true));
    //Debug map
    let mut map = MapMaker::new()
        .with_width(200)
        .with_height(100)
        .spawn();
    let mut fov_map = FovEngine::new(&map);
    fov_map.generate_fov_map(&map);

    //Debug entities
    let player = world.create_entity()
        .with(Position {x:30, y:30})
        .with(Glyph {character:'@', fg_color:colors::WHITE})
        .with(Player {})
        .with(Camera {})
        .with(TurnState {energy:0, next_action:None})
        .with(Movement {dx:0, dy:0})
        .with(Fighter {max_hp:100, hp:100, power:10})
        .build();

    let npc = world.create_entity()
        .with(Position {x:33, y:33})
        .with(Glyph {character:'s', fg_color:colors::WHITE})
        .with(TurnState {energy:0, next_action:None})
        .with(Movement {dx:0, dy:0})
        .with(Fighter {max_hp:100, hp:50, power:10})
        .build();

    map.set_contained_actor(30,30,player.id());
    map.set_contained_actor(33,33,npc.id());

    world.add_resource(map);
    world.add_resource(fov_map);

    let combat_manager = CombatManager::new();
    world.add_resource(combat_manager);

    let mut dmap_atlas = DijkstraMapAtlas::new();
    dmap_atlas.add_dmap(DijkstraMap::new(200,100, "player".to_string()));
    dmap_atlas.add_dmap(DijkstraMap::new(200,100, "stealth".to_string()));

    world.add_resource(dmap_atlas);
    //Dispatcher
    let mut dispatcher = DispatcherBuilder::new()
        //Creer un systeme compute_fov et l'intercaler avec les relations qui vont bien, pas de raison que compute dmap attende les fcts de render.
        //Il va peut etre falloir move le joueur etc avant que le reste reflechisse, sinon rien ne marche bien
        .with(DrawMap, "draw_map", &[])
        .with(DrawActors, "draw_actors", &["draw_map"])
        .with(ReplenishEnergy, "replenish_energy", &["draw_actors"])
        .with(ComputeDijkstraMap, "compute_dmap", &["draw_actors"])
        .with(HandleInput, "handle_input", &["replenish_energy"])
        .with(ChooseAction, "choose_action", &["handle_input", "compute_dmap"])
        .with(Move, "move", &["choose_action", "handle_input"])
        .with(ProcessCombat, "combat", &["move"])
        .build();
        //Idealement:
        //DrawMap
        //DrawActors
        //ReplenishEnergy
        //HandleInput
        //PlayerAct
        //ComputeFOV
        //ComputeDijkstra
        //AIChooseAction
        //AIAct
        //ProcessCombat
    //Main loop
    while world.read_resource::<Continue>().0 {
        dispatcher.dispatch(&mut world.res);
        world.maintain();
    }
}

pub struct Continue(pub bool);