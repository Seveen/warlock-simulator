use tcod::colors;
use tcod::noise::*;
use specs::world::Index;

type TileMap = Vec<Vec<Tile>>;

#[derive(Debug)]
pub struct Map {
    pub width: i32,
    pub height: i32,
    pub tiles: TileMap,
}

impl Map {
    pub fn clear(&mut self) {
        self.tiles = vec![vec![Tile::ground(); self.height as usize]; self.width as usize];
    }

    pub fn generate(&mut self) {
        let noise = Noise::init_with_dimensions(2)
            .noise_type(NoiseType::Default)
            .hurst(0.5)
            .lacunarity(2.0)
            .init();

        for i in 0..self.width {
            for j in 0..self.height {
                if noise.get_fbm([i as f32,j as f32], 100) >= 0.5 {
                    self.tiles[i as usize][j as usize] = Tile::wall();
                }
            }
        }
    }

    pub fn encase_in_walls(&mut self) {
        for i in 0..self.width {
            for j in 0..self.height {
                if i == 0 || i == (self.width-1) || j == 0 || j == (self.height-1) {
                    self.tiles[i as usize][j as usize] = Tile::wall();
                }
            }
        }
    }

    pub fn can_walk(&self, x: i32, y: i32) -> bool {
        !self.tiles[x as usize][y as usize].blocked
    }

    pub fn can_see(&self, x: i32, y: i32) -> bool {
        !self.tiles[x as usize][y as usize].block_sight
    }

    pub fn is_explored(&self, x: i32, y: i32) -> bool {
        self.tiles[x as usize][y as usize].explored
    }

    pub fn explore(&mut self, x: i32, y: i32) {
        self.tiles[x as usize][y as usize].explored = true;
    }

    pub fn set_contained_actor(&mut self, x: i32, y: i32, idx: Index) {
        self.tiles[x as usize][y as usize].contained_actor = Some(idx);
    }

    pub fn clear_contained_actor(&mut self, x: i32, y: i32) {
        self.tiles[x as usize][y as usize].contained_actor = None;
    }

    pub fn get_actor_at(&self, x:i32, y: i32) -> Option<Index> {
        self.tiles[x as usize][y as usize].contained_actor
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Tile {
    pub character: char,
    pub fg_color: colors::Color,
    pub blocked: bool,
    pub block_sight: bool,
    pub explored: bool,
    pub contained_actor: Option<Index>
}

impl Tile {
    pub fn ground() -> Self {
        Tile {
            character: '.',
            fg_color: colors::SEPIA,
            blocked: false,
            block_sight: false,
            explored: false,
            contained_actor: None
        }
    }

    pub fn wall() -> Self {
        Tile {
            character: '7',
            fg_color: colors::GREEN,
            blocked: true,
            block_sight: true,
            explored: false,
            contained_actor: None
        }
    }
}

pub struct MapMaker {
    pub width: i32,
    pub height: i32,
}

impl MapMaker {
    pub fn new() -> MapMaker {
        MapMaker {
            width: 0,
            height: 0,
        }
    }

    pub fn with_height<'a>(&'a mut self, height: i32) ->&'a mut MapMaker {
        self.height = height;
        self
    }

    pub fn with_width<'a>(&'a mut self, width: i32) ->&'a mut MapMaker {
        self.width = width;
        self
    }

    pub fn spawn<'a>(&'a mut self) -> Map {
        let mut map = Map {
            width: self.width,
            height: self.height,
            tiles: vec![vec![Tile::ground(); self.height as usize]; self.width as usize]
        };
        map.clear();
        map.generate();
        map.encase_in_walls();
        map
    }
}

pub struct Direction {
    pub x: i32,
    pub y: i32
}