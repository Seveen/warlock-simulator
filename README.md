An attempt at a roguelike in Rust to follow along with the event at r/roguelikedevs

[Link to the event](https://www.reddit.com/r/roguelikedev/comments/8ql895/roguelikedev_does_the_complete_roguelike_tutorial/)

## Libraries used: 
- [tcod-rs](https://github.com/tomassedovic/tcod-rs)
- [specs](https://github.com/slide-rs/specs)

## How to run:
You'll need an [up to date Rust install](https://rustup.rs/)
```
git clone https://gitlab.com/Seveen/warlock-simulator.git
cd warlock-simulator
cargo run
```